package Bsic;
// outerclass
public class Mobile {
    String company  = "SAMSUNG";
}
// iNNER CLASS
class Ram {
    void display(){
        System.out.println("RAM SIZE IS 8GB ");
    }
}
class  Processor {
    void info(){
        System.out.println("PROCESSOR NAME IS SNAPDRAGON");
    }
}
  /* class MainProg {
   /* public static void main(String[] args) {
        // Encapsulation
        Mobile m = new Mobile();
        // ENCAPSULATION1
        Mobile.Ram r1 = m.new Ram();
        r1.display();
        // Encapsulation 2
        Mobile.Processor p1 = m.new Processor();
        p1.info();
    }
}
*/