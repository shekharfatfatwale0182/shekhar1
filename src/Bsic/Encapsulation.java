package Bsic;

public class Encapsulation {
    private  int empId = 101;
    private  double empSalary = 50000;

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public double getEmpSalary() {
        return empSalary;
    }

    public void setEmpSalary(double empSalary) {
        this.empSalary = empSalary;
    }
}
class MainA{
    public static void main(String[] args) {
      Encapsulation e= new Encapsulation();
       // read access
      int id = e.getEmpId();
      double salary = e.getEmpSalary();
        System.out.println("emp id "+ id);
        System.out.println("employee salary "+salary);

        // write access
        e.setEmpId(102);
        e.setEmpSalary(700000);
        System.out.println(e.getEmpId());
        System.out.println(e.getEmpSalary());
    }
}
