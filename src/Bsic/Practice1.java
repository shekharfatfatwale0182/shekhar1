package Bsic;

public class Practice1 {
    //constructor chaining
    Practice1(String universityname){
        System.out.println("UNIVERSITY NAME "+universityname);
    }
}
class Practice2 extends Practice1 {
    Practice2(String universityname ,String collegename){
        super(universityname);
        System.out.println("college name "+ collegename);
    }
}
class Practice3 extends Practice2 {
    Practice3(String universityname ,String collegename , String departmentname){
        super(universityname,collegename);
        System.out.println("DEPARTMENT NAME"+ "\t"+ departmentname);
    }
}
class MainApp12{
    public static void main(String[] args) {
        Practice3 p1 = new Practice3("SOLAPUR UNIVERSITY" , "WIT" , "ENTC");

    }
}