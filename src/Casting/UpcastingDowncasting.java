package Casting;

public class UpcastingDowncasting {
    //superclass
    // without upcasting we can not perfom downcasting
    void casting(){
        System.out.println("superclass");
    }
}
 // subclass
class Demo extends  UpcastingDowncasting {
    void demo(){
        System.out.println("subclass");
    }
}
class Main{
    public static void main(String[] args) {
        UpcastingDowncasting u  =  new Demo ();  // upcasting
        u.casting();
        Demo d = (Demo) new UpcastingDowncasting();
        d.demo();
        d.casting();
    }
}
