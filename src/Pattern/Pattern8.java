package Pattern;

public class Pattern8 {
    public static void main(String[] args) {
        int row = 5;
        int col = 1;
        for (int i = 0; i <= 5; i++) {
                for (int j = 0; j < col; j++) {
                    System.out.print("*" + " ");
                }
                System.out.println();
                col++;
        }
    }
}