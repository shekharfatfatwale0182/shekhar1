package ConstructorChaining;

public class University {
    University(String universityName){
        System.out.println("SUPER CLASS");
        System.out.println("UNIVERSITY NAME IS " + universityName);
    }
}
class College extends University {
    College(String universityName,String collegeName){
        super(universityName);
        System.out.println("SUB CLASS ");
        System.out.println("College Name "+collegeName);
    }
}
class Departmaent extends College{
    Departmaent(String universityName,String collegeName,String departmentName){
        super(universityName,collegeName);
        System.out.println("DEPARTMENT NAME IS "+departmentName);
    }
}
class MainApp5{
    public static void main(String[] args) {
        Departmaent d1 = new Departmaent("SOLAPUR UNIVERSITY","WIT","ENTC");

    }
}