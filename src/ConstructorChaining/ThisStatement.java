package ConstructorChaining;

public class ThisStatement {
    ThisStatement(String name){
        System.out.println("student name"+name );
    }
    ThisStatement(char surname){
        this("shekhar");
        System.out.println("CHAR OF SURNAME"+surname );
    }
    ThisStatement(int a){
        System.out.println(a);
    }
}
class Subclass extends ThisStatement{
    Subclass(){
        super(23);
        System.out.println("SUBCLASS");
    }
}
class MainApp6{
    public static void main(String[] args) {
        Subclass s1 = new Subclass();
    }
}