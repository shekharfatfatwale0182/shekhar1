package Series;

public class CountSpyNumber {
    public static void main(String[] args) {
        for (int i = 10 ; i<=10000 ; i++){
            int a = i;
            int temp = a;
            int sum = 0;
            int mul = 1;
            while (a!=0){
                int r =a%10;
                sum+=r;
                mul = mul*r;
                a/=10;
            }
            if (mul == sum){
                System.out.println(i);
            }
        }
    }
}
