package Series;

public class CountZero {
    public static void main(String[] args) {
        int l = 11000;
        int count = 0;
        while (l!=0){
            int r = l%10;
            if (r == 0){
                count++;
            }
            l=l/10;
        }
        System.out.println(count);
    }
}
