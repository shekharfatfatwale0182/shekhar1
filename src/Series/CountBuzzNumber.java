package Series;

import javax.swing.*;

public class CountBuzzNumber {
    public static void main(String[] args) {
        for (int j = 1; j <= 1000; j++) {
            int a = j;
            if (a % 10 == 7 || a % 7 == 0) {
                System.out.print(" "+a+" ");
            }
        }
    }
}