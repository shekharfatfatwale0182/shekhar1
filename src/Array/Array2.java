package Array;

import javax.swing.*;

public class Array2 {
    public static void main(String[] args) {
        int[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
        int sum = 0;
        for (int a:arr){
            sum+=a;
        }
        System.out.println("SUM OF ALL NUMBER IS " + sum);
        double avg = sum/ arr.length;
        System.out.println("AVERAGE OF NUMBERS IS " + avg);
    }
}