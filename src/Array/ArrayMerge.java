package Array;

public class ArrayMerge {
    public static void main(String[] args) {
        int [] arr  = {1,2,3,4};
        int [] arr1 = {5,6,7,8,9};
        int n1 = arr.length;
        int n2 = arr1.length;
        int [] arr3 = new int [n1+n2];
        int idx=0;
        for (int a:arr){
            arr3[idx] =a;
            idx++;
        }
        for (int a :arr1){
            arr3[idx] = a;
            idx++;
        }
        for (int j = 0 ; j< arr3.length;j++){
            System.out.print(" "+arr3[j]);     // 1,2,3,4,5,6,7,8,9
        }

    }
}
