public class Encapsulation {
    private int empId = 101;
    private  double empSalary = 50000;
         //Read Access
    public int getEmpId() {
        return empId;
    }
         // Write Access
    public void setEmpId(int empId) {
        this.empId = empId;
    }
         // read Access
    public double getEmpSalary() {
        return empSalary;
    }
         // Write Access
    public void setEmpSalary(double empSalary) {
        if (empSalary > 20000) {
            this.empSalary = empSalary;
        } else {
            System.out.println("INVALID SALARY");
        }

    }
}
