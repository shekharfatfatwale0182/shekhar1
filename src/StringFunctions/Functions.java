package StringFunctions;

public class Functions {
    public static void main(String[] args) {
        String name = "Software Tester";
        System.out.println(name.length());
        System.out.println(name.charAt(5));
        System.out.println(name.indexOf('w'));
        System.out.println(name.lastIndexOf('T'));
        System.out.println(name.contains("ertrs"));
        System.out.println(name.startsWith("Soft"));
        System.out.println(name.endsWith("r"));
        System.out.println(name.substring(13,14));
        System.out.println(name.toUpperCase());
        System.out.println(name.toLowerCase());
    }
}
