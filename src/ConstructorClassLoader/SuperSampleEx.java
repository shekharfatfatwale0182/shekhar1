package ConstructorClassLoader;

public class SuperSampleEx {
    SuperSampleEx(int a){
        System.out.println("Superclass");
        System.out.println(a);
    }
}
class Subclass extends SuperSampleEx{
    Subclass(){
        super(24);
        System.out.println("subclass");
    }
}
class Main{
    public static void main(String[] args) {
        Subclass s1 = new Subclass();
    }
}
