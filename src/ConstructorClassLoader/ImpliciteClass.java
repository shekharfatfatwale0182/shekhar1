package ConstructorClassLoader;

public class ImpliciteClass {
    // superclass
    ImpliciteClass(){
        System.out.println("super class");
    }
}
   // subclass
class Sample extends ImpliciteClass{
    public Sample(){
        System.out.println("subclass");
    }
}
class  MainApp{
    public static void main(String[] args) {
        Sample s1  = new Sample();
    }
}
