package Oops;

import java.util.Scanner;

public class HighestNumber {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("enter the first number");
        int n1 = sc1.nextInt();
        System.out.println("enter the second number");
        int n2 = sc1.nextInt();
        System.out.println("enter the third number");
        int n3 = sc1.nextInt();
        Highest h1 = new Highest();
        h1.highest1(n1,n2,n3);
        h1.highest2(n1,n2,n3);
        h1.highest3(n1,n2,n3);

    }

}
class Highest {
    void highest1(int n1, int n2, int n3) {
        if (n1 > n2 && n1 > n3) {
            System.out.println("n1 is greatest ");
        }
    }

    void highest2(int n1, int n2, int n3) {
        if (n2 > n1 && n2 > n3) {
            System.out.println("n2 is greatest ");
        }
    }

    void highest3(int n1, int n2, int n3) {
        if (n3 > n2 && n3 > n1) {
            System.out.println("n3 is greatest ");
        }
    }
}



