package Oops;

import java.util.Scanner;

public class MethodOverloding {
    String name = "shekhar";
    int age = 23;
    long ctctno = 8411828350l;

    void display(String userName) {
        if (name.equalsIgnoreCase(userName)) {
            System.out.println("ENTER THE NAME "+name);
        }
        else {
            System.out.println("record is not present");
        }
    }
    void display(int userAge){
        if (age == userAge){
            System.out.println("enter user AGE"+ age);
            System.out.println("RECORD IS PRESENT");
        }
    }
}
class MainApp7 {
    public static void main(String[] args) {
        MethodOverloding m1 = new MethodOverloding();
        Scanner sc1 = new Scanner(System.in);
        System.out.println("ENTERR YOUR CHOICE");
        System.out.println("1 : FOR NAME WISE SELECTION"+"\n"+ "2 : FOR AGE WISE SELECTION" );
        int choice = sc1.nextInt();
        if (choice == 1){
            System.out.println("enter name");
            String name = sc1.next();
            m1.display(name);
        } else if (choice == 2) {
            System.out.println("enter your age");
            int age = sc1.nextInt();
            m1.display(age);
        }
        else {
            System.out.println("RECCORD NOT MATCHED");
        }
    }
}