package Oops;

import java.util.Scanner;
import java.util.concurrent.Callable;

public class hirarchical {
    void employee(int empid , double sal){
        System.out.println("EMPLOYEE ID IS "+empid);
        System.out.println("EMPLOYEE SAL IS "+sal);
    }
}
class  Permanent extends hirarchical{
    void info(String desgn){
        System.out.println("designation is "+desgn);
    }
}
class Contract extends hirarchical{
    void display(int month){
        System.out.println("designation is "+month);

    }
}

class MainApp2 {
    public static void main(String[] args) {
        Permanent p1 = new Permanent();
        p1.employee(2,20000);
        p1.info("analyst");
        Contract c1 = new Contract();
        c1.employee(4,30000);
        c1.display(23);

    }
}
