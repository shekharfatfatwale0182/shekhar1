package Oops;

import java.util.Scanner;

public class ReverseNumber {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("enter start number");
        int n1 = sc1.nextInt();
        System.out.println("enter last number");
        int n2 = sc1.nextInt();
        int n3 = 0;
        for (int i = n2 ; i>=n1 ; i--){
            System.out.print(i);
        }
    }
}
