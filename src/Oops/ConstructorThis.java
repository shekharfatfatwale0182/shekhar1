package Oops;

public class ConstructorThis {
    ConstructorThis(int a){
        System.out.println(a);
    }

    ConstructorThis(char b){
        this(24);
        System.out.println(b);
    }
    ConstructorThis(String name){
        this('J');
        System.out.println(name);
    }
}
// SUBCLASS
class Sub extends  ConstructorThis{
    Sub(){
        super("shekhar");
    }
}
class  MainApp6 {
    public static void main(String[] args) {
        Sub s1 = new Sub();
    }
}