package Oops;

import java.util.Scanner;

public class Overloding_Method {
    void sellproduct(int qty , double price){
       double  total = qty*price;
        System.out.println("TOTAL PRICE "+total);
    }
}
       //subclass
class Flipcart extends Overloding_Method{
    void sellproduct(int qty , double price){
        double total = qty*price;
        // apply 10 % discount
        double finalAmt = total - total*0.1;
        System.out.println("FINAL AMOUNT  "+finalAmt);
    }
}
class Amazone extends Overloding_Method {
    void sellproduct(int qty, double price) {
        double total = qty * price;
        // apply 15 % discount
        double finalAmt = total - total * 0.15;
        System.out.println("FINAL AMOUNT  " + finalAmt);
    }
}
class MainApp8{
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("ENTER QTY");
        int qty = sc1.nextInt();
        System.out.println("ENTER PRICE");
        double price = sc1.nextInt();
        System.out.println("1 : FOR FLIPCART \n 2 : FOR AMAZONE");
        System.out.println("ENTER YOUR CHOICE ");
        int choice = sc1.nextInt();
        if (choice == 1){
            Flipcart f1 = new Flipcart();
            f1.sellproduct(qty,price);
        }
    }
}
