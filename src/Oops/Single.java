package Oops;
// superclass
public class Single {
    void display(){
        System.out.println("parent property");
    }
}
// subclass
class  Child extends Single{
    void info(){
        System.out.println("child property");
    }
}
// Main class
class MainApp{
    public static void main(String[] args) {
        Child c1 = new Child();
        c1.display();
        c1.info();
    }
}
