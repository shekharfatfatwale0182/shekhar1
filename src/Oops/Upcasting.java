package Oops;

import java.sql.SQLOutput;
import java.util.Scanner;

//SUPERCLASSSSSSSSSSSSS
public class Upcasting {
    void get(){
        System.out.println("to display");
    }
    void display(int qty , double price){
         double total  = qty*price;
        System.out.println("total bill "+total);
    }
}
                             // SUBCLASSSSS
class laptop extends  Upcasting {
    void get(){
        System.out.println("MACHINE IS LAPTOP");
    }
    void  display(int qty , double price){
       double total = qty*price;
                   // apply 15% gst
        double finalAmt  = total+total*0.15;
        System.out.println("FINAL AMOUNT IS"+finalAmt);
    }
}
                           // SUBCLASSSSSSSSSS
class  Projector extends  Upcasting{
    void  get(){
        System.out.println("MACHINE TYPE IS PROJECTOR ");
    }

                               @Override
                               void display(int qty, double price) {
                                   double total = qty*price;
                                   // apply 10% gst
                                   double finalAmt  = total+total*0.1;
                                   System.out.println("FINAL AMOUNT IS"+finalAmt);
                               }
                           }
class  MainApp9{
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        Upcasting u1 = null;
        System.out.println("enter qty");
        int qty = sc1.nextInt();
        System.out.println("enter price");
        int price = sc1.nextInt();
        System.out.println(" 1 : FOR LAPTOTP  \n  2 : for projector");
        System.out.println("enter your choice");
        int choice = sc1.nextInt();
        if (choice == 1){
            u1 = new laptop();
        } else if (choice == 2) {
            u1 = new Projector();
        }
        u1.get();
        u1.display(qty,price);

    }
}