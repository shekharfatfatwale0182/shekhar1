package Interference;
@FunctionalInterface
 interface UPI {
    void transferAmount(double amt);
}
@FunctionalInterface
 interface Wallet{
    void makeBillPayment(double amt);
}
//implementation class
class  Calculation implements  UPI , Wallet{

    @Override
    public void transferAmount(double amt) {
        System.out.println("TRANSFER AMOUNT IS " + amt);
    }

    @Override
    public void makeBillPayment(double amt) {
        System.out.println("TRANSFER AMOUNT IS " + amt);
    }
}
//Main class
class  MainDemo {
    public static void main(String[] args) {
        Calculation c1  =  new Calculation();
        c1.makeBillPayment(50000);
        c1.transferAmount(70000);
    }
}
