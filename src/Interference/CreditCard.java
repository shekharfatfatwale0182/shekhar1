package Interference;

public interface CreditCard {
    void getType();
    void withdraw(double amt);
}
class Visa implements CreditCard{

    @Override
    public void getType() {
        System.out.println("CARD TYPE IS VISA");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("WITHDRAW AMOUNT " + amt);
    }
}
class  MasterCart implements CreditCard{

    @Override
    public void getType() {
        System.out.println("CART TYPE IS MASTER CART");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("WITHDRAW DOLLAR" + amt);
    }
}
class  MainClass{
    public static void main(String[] args) {
        CreditCard card =  null;
        card = new Visa();
        card.getType();
        card.withdraw(230000);
        System.out.println("===================");
        card = new Visa();
        card.getType();
        card.withdraw(50);
    }
}

