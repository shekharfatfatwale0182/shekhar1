package Interference;

import java.sql.SQLOutput;

public class FrontEND {
    void details(){
        System.out.println("\"DESIGN USER INTERFERENCE WITH HTMLL , CSS , JAVA SCRIPT\"");
    }
}
              //superInterference
  interface  BackEnd {
    void devlopProgramm(String lang);
}

     //SUPER INTERFERENCE
interface  DataBase {
    void database(String dbVendor);
}
class  Software extends FrontEND implements BackEnd , DataBase{

    @Override
    public void devlopProgramm(String lang) {
        System.out.println("DECLOP APP BY USING  " + lang + "LANGUAGE");
    }

    @Override
    public void database(String dbVendor) {
        System.out.println("DESIGN DATA BASE WITH "+dbVendor);
    }
}
class MainDEmo{
    public static void main(String[] args) {
        Software s = new Software();
        s.devlopProgramm("PYTHONE");
        s.database("ORACLE");
    }
}