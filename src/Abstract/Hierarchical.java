package Abstract;

import java.util.Scanner;

public abstract class Hierarchical {
    //superclass
    void employee(){
        System.out.println("EMPLOYEEE CLASS");
    }
   abstract void designation();
   abstract  void salary();
}
               ////////////////////////////subclass//////////////////////////////////
class  Manager extends Hierarchical {
    void designation(){
        System.out.println("DESIGNATION IS MANAGER");
    }
    void salary(){
        System.out.println("SALARYY IS 300000");
    }

}
class  Watchman extends Hierarchical {
    void designation(){
        System.out.println("DESIGNATION IS WATCHMAN");
    }
    void salary(){
        System.out.println("SALARYY IS 30000");
    }
}
/////////////////////////MAIN CLASS //////////////////////////////
class MainApa13{
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("ENTER YOUR CHOICE");
        System.out.println("1 : for manager /n 2 : for watchman");
        int choice = sc1.nextInt();
        Hierarchical h1 = null;
        if (choice == 1){
          h1 = new Manager();
        } else if (choice == 2) {
            h1 = new Watchman();
        }
        else{
            System.out.println("wrong choice");
        }
        h1.designation();
        h1.salary();
    }
}