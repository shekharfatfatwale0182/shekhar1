package Abstract;
//superclass
public abstract class SingleLevel {
 void info(){                 // concrete
     System.out.println("super class method");
 }
 abstract void display();    //abstract
}
//SUBCLASS
class Demo extends  SingleLevel {
    @Override
    void display() {
        System.out.println("SUBCLASS METHOD");
    }
}
class MainApp {
    public static void main(String[] args) {
        Demo d = new Demo();
        d.display();
        d.info();
    }
}
