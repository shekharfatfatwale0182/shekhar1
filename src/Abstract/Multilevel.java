package Abstract;

public abstract class Multilevel {
    //Superclass
    void test(){
        System.out.println("INSTAGRAM METHOD");
    }
    abstract void post();
    abstract  void stories();
    abstract void reels();
}
 abstract class Instagram1 extends Multilevel{
    void post(){
        System.out.println("POST THE PICTURE");
    }
 }
 abstract  class Instagram2 extends Instagram1{
    void stories(){
        System.out.println("SHARE STORIES");
    }
 }
  class Instagram3 extends Instagram2{
    void reels(){
        System.out.println("SHARE REELS");
    }
  }
//MAIN CLASS
class  Main1 {
    public static void main(String[] args) {
        Instagram3 i3 =  new Instagram3();
        i3.test();
        i3.post();
        i3.stories();
        i3.reels();
    }
}